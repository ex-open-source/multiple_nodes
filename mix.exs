defmodule MultipleNodes.Mixfile do
  use Mix.Project

  def application, do: [extra_applications: [:logger], mod: {MultipleNodes.Application, []}]

  def project, do: [
    app: :multiple_nodes,
    build_embedded: Mix.env == :prod,
    deps: [{:excoveralls, "~> 0.7", only: :test}, {:phoenix_pubsub, "~> 1.0"}],
    elixir: "~> 1.5",
    preferred_cli_env: [
      "coveralls": :test,
      "coveralls.detail": :test,
      "coveralls.post": :test,
      "coveralls.html": :test
    ],
    source_url: "https://gitlab.com/ex-open-source/multiple_nodes",
    start_permanent: Mix.env == :prod,
    test_coverage: [tool: ExCoveralls],
    version: "1.0.0",
  ]
end
