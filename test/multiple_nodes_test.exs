defmodule MultipleNodesTest do
  use ExUnit.Case

  alias MultipleNodes.Cluster

  @node_names [:first, :second, :third]

  test "example" do
    data = "sample"
    Cluster.spawn(@node_names) # spawns and connect nodes

    MultipleNodes.example(data)
    assert data == MultipleNodes.sample

    Process.sleep(1000) # wait for sync all nodes

    for node_name <- @node_names do
      # check if all nodes have data
      assert data == Cluster.block_call(node_name, MultipleNodes, :sample, [])
    end
  end
end
