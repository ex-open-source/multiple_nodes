# MultipleNodes

Example project for: https://github.com/parroty/excoveralls/issues/110

## Steps to reproduce:

```bash
git clone git@gitlab.com:ex-open-source/multiple_nodes.git
cd multiple_nodes
mix deps.get
mix coveralls.html
```
