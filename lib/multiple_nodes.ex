defmodule MultipleNodes do
  use GenServer

  alias Phoenix.PubSub

  # init

  def init(state) do
    PubSub.subscribe(:multiple_nodes, "example")
    {:ok, state}
  end

  # server api

  def start_link(_args), do: GenServer.start_link(__MODULE__, [], name: __MODULE__)

  def example(data), do: GenServer.cast(__MODULE__, data)

  def sample, do: GenServer.call(__MODULE__, :sample)

  # handlers

  def handle_call(:sample, _from, state), do: {:reply, List.first(state), state}

  def handle_cast(request, old_state) do
    # here we are separating cast code and broadcast_from call:
    {state, data} = do_cast(request, old_state)
    PubSub.broadcast_from(:multiple_nodes, self(), "example", data)
    {:noreply, state}
  end

  defp do_cast(data, state), do: {[data | state], data}

  def handle_info(data, old_state) do
    {state, ^data} = do_cast(data, old_state)
    # no broadcast_from called - i.e. prevent sending data infinite loop
    {:noreply, state}
  end
end
