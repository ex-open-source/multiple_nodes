defmodule MultipleNodes.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    pub_sub = supervisor(Phoenix.PubSub.PG2, [:multiple_nodes, []])
    Supervisor.start_link([pub_sub, MultipleNodes], [strategy: :one_for_one, name: MultipleNodes.Supervisor])
  end
end
